<?php

class User_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function get_all()
    {
        return $this->db->get('user')->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('user')->row();
    }

    public function insert($username,$password,$hak_akses)
    {
        $data = array(
            'username' => $username,
            'password' => md5($password),
            'hak_akses' => $hak_akses,
        );
        return $this->db->insert('user',$data);
    }

    public function update($id,$username,$hak_akses)
    {
        $this->db->where('id',$id);
        $data = array(
            'username' => $username,
            'password' => md5($password),
            'hak_akses' => $hak_akses,
        );
        return $this->db->update('user',$data);
    }
    public function update_password($id,$password)
    {
        $this->db->where('id',$id);
        $data = array(
            'password' => md5($password)
          );

        return $this->db->update('user',$data);
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        return $this->db->delete('user');
    }

    public function cek_username($username)
    {
      $this->db->from('user');
      $this->db->where('username',$username);
      $query = $this->db->get();
      $data = $query->result();
      $ada_data = count($data) > 0;
      return $ada_data;
    }

    public function cek_akun($username,$password)
    {
      $this->db->from('user');
      $this->db->where('username',$username);
      $this->db->where('password',$password);
      $query = $this->db->get();
      $data = $query->result();
      $ada_data = count($data) > 0;
      return $ada_data;
    }

    public function get_akun($username,$password)
    {
      $this->db->from('user');
      $this->db->where('username',$username);
      $this->db->where('password',$password);
      $query = $this->db->get();
      $data = $query->row();
      return $data;
    }
}
