<?php

class Kabupaten_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function get_all()
    {
        $this->db->select('*, kabupaten.id as id_kabupaten');
        $this->db->from('kabupaten');
        $this->db->join('provinsi','kabupaten.id_provinsi=provinsi.id');
        return $this->db->get()->result();
    }

    public function get_by_id($id)
    {
        $this->db->select('*, kabupaten.id as id_kabupaten');
        $this->db->from('kabupaten');
        $this->db->join('provinsi','kabupaten.id_provinsi=provinsi.id');
        $this->db->where('kabupaten.id',$id);
        return $this->db->get()->row();
    }

    public function get_kabupaten_where_provinsi($id)
    {
        $this->db->select('*, kabupaten.id as id_kabupaten');
        $this->db->from('kabupaten');
        $this->db->join('provinsi','kabupaten.id_provinsi=provinsi.id');
        $this->db->where('kabupaten.id_provinsi',$id);
        return $this->db->get()->result();
    }

    public function insert($id_provinsi,$nama_kabupaten,$jumlah)
    {
        $data = array(
            'id_provinsi' => $id_provinsi,
            'nama_kabupaten' => $nama_kabupaten,
            'jumlah' => $jumlah,
        );
        return $this->db->insert('kabupaten',$data);
    }

    public function update($id,$id_provinsi,$nama_kabupaten,$jumlah)
    {
        $this->db->where('id',$id);
        $data = array(
            'id_provinsi' => $id_provinsi,
            'nama_kabupaten' => $nama_kabupaten,
            'jumlah' => $jumlah,
        );
        return $this->db->update('kabupaten',$data);
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        return $this->db->delete('kabupaten');
    }

}
