<?php

class Provinsi_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function get_all()
    {
        return $this->db->get('provinsi')->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('provinsi')->result();
    }
    public function nama_provinsi($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('provinsi')->row();
    }

    public function insert($nama_provinsi)
    {
        $data = array(
            'nama_provinsi' => $nama_provinsi,
        );
        return $this->db->insert('provinsi',$data);
    }

    public function update($id,$nama_provinsi)
    {
        $this->db->where('id',$id);
        $data = array(
            'nama_provinsi' => $nama_provinsi,
        );
        return $this->db->update('provinsi',$data);
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        return $this->db->delete('provinsi');
    }

}
