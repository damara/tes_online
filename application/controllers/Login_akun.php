<?php
class Login_akun extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');

  }

  public function index()
  {
    $this->load->view('v_login');
  }

  public function login()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $cek_akun = $this->User_model->cek_akun($username,MD5($password));
    if ($cek_akun) {
      $akun = $this->User_model->get_akun($username,MD5($password));
      $data = array(
        'id' =>$akun->id,
        'usename'  => $akun->username,
        'hak_akses'=> $akun->hak_akses,
        'status_login'=> "islogin",
      );
      $this->session->set_userdata($data);
      $url = base_url($akun->hak_akses.'/Home');
      redirect($url);
    }else{
      $url= base_url().'?pesan='.bin2hex('Mohon maaf, Username dan Password anda tidak sesuai');
      redirect($url);
    }
  }


  public function logout()
  {
    $array_items = array('username');
    $this->session->unset_userdata($array_items);
    $halaman_login = base_url('');
    redirect($halaman_login);
  }

}
