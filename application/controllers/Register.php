<?php
class Register extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');
  }

  public function index()
  {
    $this->load->view('v_register');
  }

  public function store()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $hak_akses = "user";

    $cek_akun = $this->User_model->cek_username($username);
    if ($cek_akun<1) {
      $insert=$this->User_model->insert($username,$password,$hak_akses);

      $akun = $this->User_model->get_akun($username,MD5($password));
      $data = array(
        'id' =>$akun->id,
        'usename'  => $akun->username,
        'hak_akses'=> $akun->hak_akses,
        'status_login'=> "islogin",
      );
      $this->session->set_userdata($data);
      $url = base_url($akun->hak_akses.'/home').'?pesan='.bin2hex('Selamat anda telah berhasil mendaftar');
      redirect($url);
    }else{
      $url= base_url('/register').'?pesan='.bin2hex('Mohon maaf, Username sudah digunakan');
      redirect($url);
    }
  }


  public function logout()
  {
    $array_items = array('username');
    $this->session->unset_userdata($array_items);
    $halaman_login = base_url('');
    redirect($halaman_login);
  }

}
