<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('User_model');
    if($this->session->userdata('hak_akses')!="user"){
      redirect('','refresh');
    }
  }

  public function index()
  {
    $this->load->view('user/profil');
  }

  public function edit_password()
  {
    $id=$this->input->get('id');
    $data = $this->user_model->get_by_id($id);
    foreach ($data as $data) {
      echo '<div id="id">'.$data->id.'</div>';
      echo '<div id="password">'.$data->password.'</div>';
    }
  }

  public function update_password()
  {
    $profil = $this->User_model->get_by_id($this->session->userdata('id'));
    $password_lama = $this->input->post('password_lama');
    $password_baru = $this->input->post('password_baru');
    if ($profil->password == md5($password_lama)) {

      if ($this->User_model->update_password($profil->id,$password_baru)) {
        echo json_encode(array(
          'status'=>'sukses'
        ));
      }

    }else {
      echo json_encode(array(
        'status'=>'Password tidak sama'
      ));
    }
  }

}

/* End of file profil.php */
