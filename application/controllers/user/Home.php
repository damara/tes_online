<?php
class Home extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    if($this->session->userdata('hak_akses')!="user"){
      redirect('','refresh');
    }
  }

  public function index()
  {

    $this->load->view($_SESSION['hak_akses'].'/dashboard');
  }
}
/* End of file Home.php */
