<?php
class Home extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    if($this->session->userdata('hak_akses')!="admin"){
      redirect('','refresh');
    }
  }

  public function index()
  {
    $this->load->view('user/dashboard');
  }
}
/* End of file Home.php */
