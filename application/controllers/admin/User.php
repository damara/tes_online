<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('user_model');
    if($this->session->userdata('hak_akses')!="admin"){
      redirect('','refresh');
    }
  }

  public function index()
  {
    $data['user'] = $this->user_model->get_all();
    $this->load->view('user/user',$data);
  }

  public function store()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $hak_akses = $this->input->post('hak_akses');

    $insert=$this->user_model->insert($username,$password,$hak_akses);
    redirect(base_url($_SESSION['hak_akses'].'/user'));
  }

  public function edit()
  {
    $id=$this->input->get('id');

    $data = $this->user_model->get_by_id($id);
    $data_array = array($data);
    foreach ($data_array as $data) {
      echo '<div id="id">'.$data->id.'</div>';
      echo '<div id="username">'.$data->username.'</div>';
      echo '<div id="hak_akses">'.$data->hak_akses.'</div>';
    }
  }
  public function edit_password()
  {
    $id=$this->input->get('id');

    $data = $this->user_model->get_by_id($id);
    $data_array = array($data);
    foreach ($data_array as $data) {
      echo '<div id="id">'.$data->id.'</div>';
      echo '<div id="password">'.$data->password.'</div>';
    }
  }

  public function update()
  {
    $id = $this->input->post('id');
    $username = $this->input->post('username');
    $hak_akses = $this->input->post('hak_akses');

    $this->user_model->update($id,$username,$hak_akses);
    redirect(base_url($_SESSION['hak_akses'].'/user'));
  }

  public function update_password()
  {
    $id = $this->input->post('id');
    $password = $this->input->post('password');

    $this->user_model->update_password($id,$password);
    redirect(base_url($_SESSION['hak_akses'].'/user'));
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $this->user_model->delete($id);
    redirect(base_url($_SESSION['hak_akses'].'/user'));
  }

}

/* End of file user.php */
