<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Provinsi_model');
    if($this->session->userdata('hak_akses')!="admin"){
      redirect('','refresh');
    }
  }

  public function index()
  {
    $data['provinsi'] = $this->Provinsi_model->get_all();
    $this->load->view('user/provinsi',$data);
  }

  public function store()
  {
    $nama_provinsi = $this->input->post('nama_provinsi');

    $insert=$this->Provinsi_model->insert($nama_provinsi);
    redirect(base_url($_SESSION['hak_akses'].'/provinsi'));
  }

  public function edit()
  {
    $id=$this->input->get('id');
    
    $data = $this->Provinsi_model->get_by_id($id);
    foreach ($data as $data) {
      echo '<div id="id">'.$data->id.'</div>';
      echo '<div id="nama_provinsi">'.$data->nama_provinsi.'</div>';
    }
  }

  public function update()
  {
    $id = $this->input->post('id');
    $nama_provinsi = $this->input->post('nama_provinsi');

    $this->Provinsi_model->update($id,$nama_provinsi);
    redirect(base_url($_SESSION['hak_akses'].'/provinsi'));
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $this->Provinsi_model->delete($id);
    redirect(base_url($_SESSION['hak_akses'].'/provinsi'));
  }

}

/* End of file provinsi.php */
