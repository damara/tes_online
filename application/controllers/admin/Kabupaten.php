<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Kabupaten_model');
    $this->load->model('Provinsi_model');
    if($this->session->userdata('hak_akses')!="admin"){
      redirect('','refresh');
    }
  }

  public function index()
  {
    $data['list_provinsi'] = $this->Provinsi_model->get_all();
    $data['provinsi'] = $this->Kabupaten_model->get_all();
    $this->load->view('user/kabupaten',$data);
  }

  public function report_kabupaten_print_ajax()
  {
    $id_provinsi = $this->input->get('provinsi');
    $data['list_provinsi'] = $this->Provinsi_model->get_all();
    $data['provinsi'] = $this->Kabupaten_model->get_kabupaten_where_provinsi($id_provinsi);
    $this->load->view('user/print/kabupaten',$data);
  }

  public function prt()
  {
    $id_provinsi = $this->input->post('select_id_provinsi');
    
    if ($id_provinsi!=null) {
      $nama_provinsi= $this->Provinsi_model->nama_provinsi($id_provinsi);
      $data['nama_provinsi'] = $nama_provinsi->nama_provinsi;
      $data['provinsi'] = $this->Kabupaten_model->get_kabupaten_where_provinsi($id_provinsi);
      $this->load->view('user/print/print_kabupaten',$data);
    }else {
      $data['nama_provinsi'] = "Keseluruhan";
      $data['provinsi'] = $this->Kabupaten_model->get_all();
      $this->load->view('user/print/print_kabupaten',$data);
    }
  }
  public function print_per_kabupaten()
  {
    $id_kabupaten = $this->input->get('id');
    $data['kabupaten'] = $this->Kabupaten_model->get_by_id($id_kabupaten);
    $this->load->view('user/print/print_per_kabupaten',$data);
  }

  public function store()
  {
    $id_provinsi = $this->input->post('id_provinsi');
    $nama_kabupaten = $this->input->post('nama_kabupaten');
    $jumlah = $this->input->post('jumlah');
    $insert=$this->Kabupaten_model->insert($id_provinsi,$nama_kabupaten,$jumlah);
    redirect(base_url($_SESSION['hak_akses'].'/kabupaten'));
  }

  public function edit()
  {
    $id=$this->input->get('id');
    $kabupaten = $this->Kabupaten_model->get_by_id($id);
    $data_array = array($kabupaten);
    foreach ($data_array as $data) {
      echo '<div id="id">'.$data->id_kabupaten.'</div>';
      echo '<div id="nama_kabupaten">'.$data->nama_kabupaten.'</div>';
      echo '<div id="jumlah">'.$data->jumlah.'</div>';
      echo '<div id="id_provinsi">'.$data->id_provinsi.'</div>';
    }
  }

  public function update($id_permintaan)
  {
    $id = $this->input->post('id');
    $id_provinsi = $this->input->post('edit_id_provinsi');
    $nama_kabupaten = $this->input->post('edit_nama_kabupaten');
    $jumlah = $this->input->post('edit_jumlah');
    $this->Kabupaten_model->update($id,$id_provinsi,$nama_kabupaten,$jumlah);
    redirect(base_url($_SESSION['hak_akses'].'/kabupaten'));
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $this->Kabupaten_model->delete($id);
    redirect(base_url($_SESSION['hak_akses'].'/kabupaten'));
  }

}

/* End of file kabupaten.php */
