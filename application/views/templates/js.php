
<script src="<?php echo base_url('/assets/js/jquery-3.2.1.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/jquery.dataTables.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('/assets/js/dataTables.bootstrap.min.js') ?>"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script> -->
<!--   Core JS Files   -->
<script src="<?php echo base_url('/assets/js/material.min.js')?>" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url('/assets/js/chartist.min.js')?>"></script>
<!--  Dynamic Elements plugin -->
<script src="<?php echo base_url('/assets/js/arrive.min.js')?>"></script>
<!--  PerfectScrollbar Library -->
<script src="<?php echo base_url('/assets/js/perfect-scrollbar.jquery.min.js')?>"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url('/assets/js/bootstrap-notify.js')?>"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url('/assets/js/material-dashboard.js?v=1.2.0')?>"></script>

<script type="text/javascript">
$(function(){
 $('table').DataTable({
   "language":{
        "lengthMenu":"Tampilkan _MENU_ data per halaman.",
        "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
        "zeroRecords":"Tidak ditemukan data yang sesuai.",
        "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
        "search":"Pencarian",
        "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
        "thousands":".",
        "emptyTable":"Tidak ada data yang ditampilkan",
        "paginate":{
          "first":'<button class="btn btn-sm btn-default"> << </button>',
          "last":'<button class="btn btn-sm btn-default"> >> </button>',
          "next":'<button class="btn btn-sm btn-default"> > </button>',
          "previous":'<button class="btn btn-sm btn-default"> < </button>'
        }
      }
 });
});
</script>
