<!doctype html>
<html lang="en">

<head>
  <title>Tes Online - <?php echo @$judul ?></title>
  <?php $this->load->view('templates/icon'); ?>
  <?php $this->load->view('templates/css'); ?>
</head>


<!-- //header-ends -->
<body>
    <div class="wrapper">
        <?php $hak_akses = $_SESSION['hak_akses']?>
      <?php $this->load->view('templates/sidebar/'.$hak_akses); ?>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
              <?php $this->load->view('templates/header'); ?>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="grey">
                                    <h4 class="title"><?php echo @$judul ?></h4>
                                    <p class="category"><?php echo @$subjudul ?></p>
                                </div>
                                <div class="card-content table-responsive">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
