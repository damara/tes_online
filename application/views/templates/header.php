<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"> Tes Online</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
            <li>
            </li>
            <li>
              <a href="<?php echo base_url('login_akun/logout')?>">
                  <i class="material-icons">power_settings_new</i>
              </a>
            </li>
        </ul>
    </div>
</div>
