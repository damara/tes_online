<div class="sidebar" data-color="purple" data-image="<?php echo base_url('/assets/img/sidebar-1.jpg')?>">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <div align="center">
        </div>
        <a href="#" class="simple-text">
          Tes Online
            <br>
            <small>
                Admin
            </small>
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li>
                <a href="<?php echo base_url($_SESSION['hak_akses'].'/home');?>">
                    <i class="material-icons">dashboard</i>
                    <p>Home</p>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url($_SESSION['hak_akses'].'/user');?>">
                    <i class="material-icons">person</i>
                    <p>User</p>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url($_SESSION['hak_akses'].'/provinsi');?>">
                    <i class="material-icons">person</i>
                    <p>Provinsi</p>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten');?>">
                    <i class="material-icons">person</i>
                    <p>Kabupaten</p>
                </a>
            </li>
        </ul>
    </div>
</div>
