<?php
$data['hak_akses'] = $_SESSION['hak_akses'].''; //hak akses user
$data['judul'] = 'Provinsi'; //judul halaman
$data['subjudul'] = 'Provinsi'; //subjudul halaman
?>
<?php $this->load->view('templates/top',$data); ?>

<?php if ($_SESSION['hak_akses'] == "admin" ): ?>
<button class="btn btn-success" data-toggle="modal" data-target="#modal_tambah_provinsi">
  <i class="material-icons">add</i>Tambah
</button>
<?php endif; ?>

<br>
<table id="example1" class="table">
  <thead class="text-primary">
    <th>No</th>
    <th>Provinsi</th>
    <?php if ($_SESSION['hak_akses'] == "admin" ): ?>
    <th>Aksi</th>
    <?php endif; ?>
  </thead>
  <tbody>
    <?php $no=1; foreach (@$provinsi as $key): ?>
      <tr>
        <td><?php echo $no;?></td>
        <td><?php echo @$key->nama_provinsi;?></td>
        <?php if ($_SESSION['hak_akses'] == "admin" ): ?>
        <td>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/Provinsi/delete/'); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $key->id;?>">
            <button class="btn btn-sm btn-warning" type="button" onclick="edit_provinsi('<?php echo $key->id;?>')">Edit</button>
            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          </form>
        </td>
      <?php endif; ?>
      </tr>
      <?php $no++; endforeach; ?>
    </tbody>
  </table>

  <!-- bagian akhir konten -->
  <?php $this->load->view('templates/bottom'); ?>

  <!-- Modal tambah provinsi -->
  <div class="modal fade ui-front" id="modal_tambah_provinsi" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Tambah Provinsi</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/provinsi/store/')?>"method="post">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Provinsi</label>
                  <input type="text" class="form-control " name="nama_provinsi" id="nama_provinsi" required="" placeholder="Nama Provinsi">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="simpan_provinsi" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Tambah provinsi -->
  \
  <!-- Modal tambah provinsi -->
  <div class="modal fade ui-front" id="modal_edit_provinsi" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Edit Provinsi</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/provinsi/update/')?>" method="post">
            <input type="hidden" class="form-control " name="id" id="id" required="" placeholder="">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Provinsi</label>
                  <input type="text" class="form-control " name="nama_provinsi" id="edit_nama_provinsi" required="" placeholder="Nama Provinsi">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Edit provinsi -->

  <script type="text/javascript">
  function edit_provinsi(id) {
    $.ajax({
      type: 'get',
      url : '<?php echo base_url($_SESSION['hak_akses'].'/Provinsi/edit?id=')?>'+id,

      success: function(hasil) {
        $response = $(hasil);
        var id = $response.filter('#id').text();
        var nama_provinsi = $response.filter('#nama_provinsi').text();

        //menampilkan ke modal
        $('#id').val(id);
        $('#edit_nama_provinsi').val(nama_provinsi);
        $('#modal_edit_provinsi').modal('show');
      }
    });
  }
</script>
