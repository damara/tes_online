<?php
$data['hak_akses'] = $_SESSION['hak_akses'].''; //hak akses user
$data['judul'] = 'Profil'; //judul halaman
$data['subjudul'] = 'Profil'; //subjudul halaman
?>
<?php $this->load->view('templates/top',$data); ?>

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <label class="control-label" for="masukkanPesan">Username</label>
        <input type="text" class="form-control " name="username" id="username" value="<?php echo $this->session->userdata('usename');?>" readonly>
        <button class="btn btn-sm btn-info" type="button" data-toggle="modal"
        data-target="#modal_edit_password">Ubah Password</button>

      </div>
    </div>
  </div>
</div>

<!-- bagian akhir konten -->
<?php $this->load->view('templates/bottom'); ?>

<!-- Modal edit password -->
<div class="modal fade ui-front" id="modal_edit_password" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Tutup</span>
        </button>
        <h4 class="modal-title" id="labelModalKu">Edit Password</h4>
      </div>

      <!-- Modal Body -->
      <div class="modal-body ui-front" >
        <p class="statusMsg"></p>
        <form method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label" for="masukkanPesan">Password Lama</label>
                <input type="text" class="form-control " name="password_lama" id="password_lama" required="" placeholder="Masukkan Password baru">
                <label class="control-label" for="masukkanPesan">Password Baru</label>
                <input type="text" class="form-control " name="password_baru" id="password_baru" required="" placeholder="Masukkan Password baru">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="submit" id="simpan_password" class="btn btn-primary submitBtn">Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>

      <!-- Modal Footer -->
    </div>
  </div>
</div>
<!-- end Modal Edit password -->

<script type="text/javascript">
$('#simpan_password').on('click',function (){
  var password_lama = $('#password_lama').val();
  var password_baru = $('#password_baru').val();
  $.ajax({
    type : "POST",
    url  : "<?php echo base_url($_SESSION['hak_akses'].'/profil/update_password/')?>",
    dataType : "JSON",
    data : {password_lama:password_lama,password_baru:password_baru},
    success: function(data){
      if (data.status == 'Password tidak sama') {
        alert('Password tidak sama');
        location.href = "<?php echo base_url($_SESSION['hak_akses'].'/profil/') ?>";
      }  else if (data.status == 'sukses') {
        alert('Selamat data berhasil disimpan');
        location.href = "<?php echo base_url($_SESSION['hak_akses'].'/profil/') ?>";
      } else {
        alert('Proses simpan gagal');
        location.href = "<?php echo base_url($_SESSION['hak_akses'].'/profil/') ?>";
      }
    }
  });
  return false;
});
</script>
