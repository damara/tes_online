<!doctype html>
<html lang="en">

<head>
  <title>Tes Online</title>
  <?php $this->load->view('templates/icon'); ?>
  <?php $this->load->view('templates/css'); ?>
</head>

<body>
    <div class="wrapper">
      <?php $this->load->view('templates/sidebar/'.$_SESSION['hak_akses']); ?>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
              <?php $this->load->view('templates/header'); ?>
            </nav>
            <div class="content">
              <?php if (isset($_GET['pesan'])): ?>
                <div class="alert alert-success"><?php echo hex2bin($_GET['pesan']) ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
              <?php endif ?>
                <div class="container-fluid">
                </div>
            </div>
            <footer class="footer">
              <?php $this->load->view('templates/footer'); ?>
            </footer>
        </div>
    </div>
</body>
<?php $this->load->view('templates/js'); ?>

</html>
