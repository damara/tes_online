<?php
$data['hak_akses'] = $_SESSION['hak_akses'].''; //hak akses user
$data['judul'] = 'Kabupaten'; //judul halaman
$data['subjudul'] = 'Penduduk Kabupaten'; //subjudul halaman
?>
<?php $this->load->view('templates/top',$data); ?>

<form class="" action="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/prt/'); ?>" method="post">

  <select class="form-control " name="select_id_provinsi" id="select_id_provinsi" onchange="rekap_provinsi()">
    <option value="">Pilih</option>
    <?php foreach ($list_provinsi as $key): ?>
      <option value="<?php echo $key->id; ?>"><?php echo $key->nama_provinsi; ?></option>
    <?php endforeach; ?>
  </select>


  <button type="submit" target="_blank" class="btn btn-info" >
    <i class="material-icons">print</i>Print
  </button>
</form>
<?php if ($_SESSION['hak_akses'] == "admin" ): ?>
<button class="btn btn-success" data-toggle="modal" data-target="#modal_tambah_kabupaten">
  <i class="material-icons">add</i>Tambah
</button>
<?php endif; ?>
<br>
<div id="display">
  <table id="example1" class="table">
    <thead class="text-primary">
      <th>No</th>
      <th>Kabupaten</th>
      <th>Jumlah</th>
      <th>Aksi</th>
    </thead>
    <tbody>
      <?php $no=1; foreach (@$provinsi as $key): ?>
        <tr>
          <td><?php echo $no;?></td>
          <td><?php echo @$key->nama_kabupaten;?></td>
          <td><?php echo @$key->jumlah;?></td>
          <td>
            <form action="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/delete/'); ?>" method="post">
              <input type="hidden" name="id" value="<?php echo $key->id_kabupaten;?>">
              <?php if ($_SESSION['hak_akses'] == "admin" ): ?>
                <button class="btn btn-sm btn-warning" type="button" onclick="edit_kabupaten('<?php echo $key->id_kabupaten;?>')">Edit</button>
                <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
              <?php endif; ?>
              <a href="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/print_per_kabupaten?id='.$key->id_kabupaten); ?>"class="btn btn-sm btn-info">Print</a>
            </form>
          </td>
        </tr>
        <?php $no++; endforeach; ?>
      </tbody>
    </table>
  </div>

  <!-- bagian akhir konten -->
  <?php $this->load->view('templates/bottom'); ?>

  <!-- Modal tambah kabupaten -->
  <div class="modal fade ui-front" id="modal_tambah_kabupaten" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Tambah Jumlah Penduduk Kabupaten</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/store/')?>"method="post">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Provinsi</label>
                  <select class="form-control " name="id_provinsi" id="id_provinsi" required>
                    <option value="">Pilih</option>
                    <?php foreach ($list_provinsi as $key): ?>
                      <option value="<?php echo $key->id; ?>"><?php echo $key->nama_provinsi; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Kabupaten</label>
                  <input type="text" class="form-control " name="nama_kabupaten" id="nama_kabupaten" required="" placeholder="Nama Kabupaten">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Jumlah</label>
                  <input type="number" class="form-control " name="jumlah" id="jumlah" required="" placeholder="Jumlah Penduduk">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="simpan_provinsi" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Tambah kabupaten -->

  <!-- Modal edit kabupaten -->
  <div class="modal fade ui-front" id="modal_edit_kabupaten" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Edit Jumlah Penduduk Kabupaten</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/update/')?>" method="post">
            <input type="hidden" class="form-control " name="id" id="id" required="" placeholder="">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Provinsi</label>
                  <select class="form-control " name="edit_id_provinsi" id="edit_id_provinsi" required>
                    <option value="">Pilih</option>
                    <?php foreach ($list_provinsi as $key): ?>
                      <option value="<?php echo $key->id; ?>"><?php echo $key->nama_provinsi; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Kabupaten</label>
                  <input type="text" class="form-control " name="edit_nama_kabupaten" id="edit_nama_kabupaten" required="" placeholder="Nama Kabupaten">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Jumlah</label>
                  <input type="number" class="form-control " name="edit_jumlah" id="edit_jumlah" required="" placeholder="Jumlah Penduduk">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Edit kabupaten -->

  <script type="text/javascript">
  function edit_kabupaten(id) {
    $.ajax({
      type: 'get',
      url : '<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/edit?id=')?>'+id,

      success: function(hasil) {
        $response = $(hasil);
        var id = $response.filter('#id').text();
        var nama_kabupaten = $response.filter('#nama_kabupaten').text();
        var jumlah = $response.filter('#jumlah').text();
        var id_provinsi = $response.filter('#id_provinsi').text();

        //menampilkan ke modal
        $('#id').val(id);
        $('#edit_nama_kabupaten').val(nama_kabupaten);
        $('#edit_jumlah').val(jumlah);
        $('#edit_id_provinsi').val(id_provinsi);
        $('#modal_edit_kabupaten').modal('show');
      }
    });
  }

  function rekap_provinsi() {
    var select_id_provinsi = $('#select_id_provinsi').val();
    var url_print = "<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/report_kabupaten_print_ajax/')?>";
    url_print += "?provinsi="+select_id_provinsi;

    $.ajax({
      url:url_print+'&display=',
      success:function(data){
        console.log(data);
        $('#display').html("<br><br>"+data);
        $('#main').attr('border','0');
        $('#main').attr('class','table table-striped table-hover');
        $('#main').dataTable();
      }
    });
  }
</script>
