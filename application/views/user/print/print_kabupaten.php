<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Provinsi <?php echo $nama_provinsi; ?></title>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <h3>Jumlah Penduduk Provinsi <?php echo $nama_provinsi; ?></h3>
          <table id="example1" border="1" class="table table-striped table-hover">
            <thead class="text-primary">
              <th>No</th>
              <th>Kabupaten</th>
              <th>Jumlah</th>
            </thead>
            <tbody>
              <?php $total=0; $no=1; foreach (@$provinsi as $key): ?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo @$key->nama_kabupaten;?></td>
                  <td><?php echo @$key->jumlah;?></td>
                </tr>
                <?php $total+=$key->jumlah?>
                <?php $no++; endforeach; ?>
                <td align="center" colspan="2">Total</td>
                <td colspan="2"><?php echo $total;?></td>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
  </html>
<!-- bagian akhir konten -->

  <script type="text/javascript">
    window.print();
  </script>
