<?php
$data['hak_akses'] = $_SESSION['hak_akses'].''; //hak akses user
$data['judul'] = 'Kabupaten'; //judul halaman
$data['subjudul'] = 'Penduduk Kabupaten'; //subjudul halaman
?>
<?php $this->load->view('templates/icon'); ?>
<?php $this->load->view('templates/css'); ?>
<?php $this->load->view('templates/js'); ?>

<br>
<table id="example1" class="table">
  <thead class="text-primary">
    <th>No</th>
    <th>Kabupaten</th>
    <th>Jumlah</th>
    <th>Aksi</th>
  </thead>
  <tbody>
    <?php $total=0; $no=1; foreach (@$provinsi as $key): ?>
      <tr>
        <td><?php echo $no;?></td>
        <td><?php echo @$key->nama_kabupaten;?></td>
        <td><?php echo @$key->jumlah;?></td>
        <td>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/delete/'); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $key->id_kabupaten;?>">
            <?php if ($_SESSION['hak_akses'] == "admin"): ?>
              <button class="btn btn-sm btn-warning" type="button" onclick="edit_kabupaten('<?php echo $key->id_kabupaten;?>')">Edit</button>
              <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
            <?php endif; ?>
            <a href="<?php echo base_url($_SESSION['hak_akses'].'/kabupaten/print_per_kabupaten?id='.$key->id_kabupaten); ?>"class="btn btn-sm btn-info">Print</a>
          </form>
        </td>
        <?php $total+=$key->jumlah?>
        <?php $no++; endforeach; ?>
      </tr>
      <td align="center" colspan="2">Total</td>
      <td colspan="2"><?php echo $total;?></td>
    </tbody>
  </table>
  <!-- bagian akhir konten -->
