<?php
$data['hak_akses'] = $_SESSION['hak_akses'].''; //hak akses user
$data['judul'] = 'User'; //judul halaman
$data['subjudul'] = 'User'; //subjudul halaman
?>
<?php $this->load->view('templates/top',$data); ?>

<button class="btn btn-success" data-toggle="modal"
data-target="#modal_tambah_user">
<i class="material-icons">add</i>Tambah
</button>

<br>
<table id="example1" class="table">
  <thead class="text-primary">
    <th>No</th>
    <th>User</th>
    <th>Password</th>
    <th>Hak Akses</th>
    <th>Aksi</th>
  </thead>
  <tbody>
    <?php $no=1; foreach (@$user as $key): ?>
      <tr>
        <td><?php echo $no;?></td>
        <td><?php echo @$key->username;?></td>
        <td><?php echo @$key->password;?></td>
        <td><?php echo @$key->hak_akses;?></td>
        <td>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/user/delete/'); ?>" method="post">
            <button class="btn btn-sm btn-warning" type="button" onclick="edit_user('<?php echo $key->id;?>')">Edit</button>
            <button class="btn btn-sm btn-info" type="button" onclick="edit_password('<?php echo $key->id;?>')">Ubah Password</button>
            <input type="hidden" name="id" value="<?php echo $key->id;?>">
            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          </form>
        </td>
      </tr>
      <?php $no++; endforeach; ?>
    </tbody>
  </table>

  <!-- bagian akhir konten -->
  <?php $this->load->view('templates/bottom'); ?>

  <!-- Modal tambah user -->
  <div class="modal fade ui-front" id="modal_tambah_user" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Tambah User</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/user/store/')?>"method="post">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">user</label>
                  <input type="text" class="form-control " name="username" id="username" required="" placeholder="Nama user">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Password</label>
                  <input type="text" class="form-control " name="password" id="password" required="" placeholder="Password">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Hak Akses</label>
                  <select class="form-control " name="hak_akses" id="hak_akses" required="">
                    <option value="">Pilih</option>
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="simpan_user" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Tambah user -->

  <!-- Modal edit user -->
  <div class="modal fade ui-front" id="modal_edit_user" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Edit user</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/user/update/')?>" method="post">
            <input type="hidden" class="form-control " name="id" id="id" required="" placeholder="">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">user</label>
                  <input type="text" class="form-control " name="username" id="edit_username" required="" placeholder="Nama user">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Hak Akses</label>
                  <select class="form-control " name="hak_akses" id="edit_hak_akses" required="">
                    <option value="">Pilih</option>
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Edit user -->

  <!-- Modal edit password -->
  <div class="modal fade ui-front" id="modal_edit_password" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Tutup</span>
          </button>
          <h4 class="modal-title" id="labelModalKu">Edit user</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body ui-front" >
          <p class="statusMsg"></p>
          <form action="<?php echo base_url($_SESSION['hak_akses'].'/user/update_password/')?>" method="post">
            <input type="hidden" class="form-control " name="id" id="edit_id" required="" placeholder="">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="masukkanPesan">Password</label>
                  <input type="text" class="form-control " name="password" id="edit_password" required=""placeholder="Password Baru">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary submitBtn">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- Modal Footer -->
      </div>
    </div>
  </div>
  <!-- end Modal Edit password -->

  <script type="text/javascript">
  function edit_user(id) {
    $.ajax({
      type: 'get',
      url : '<?php echo base_url($_SESSION['hak_akses'].'/user/edit?id=')?>'+id,

      success: function(hasil) {
        $response = $(hasil);
        var id = $response.filter('#id').text();
        var username = $response.filter('#username').text();
        var hak_akses = $response.filter('#hak_akses').text();

        //menampilkan ke modal
        $('#id').val(id);
        $('#edit_username').val(username);
        $('#edit_hak_akses').val(hak_akses);
        $('#modal_edit_user').modal('show');
      }
    });
  }

  function edit_password(id) {
    $.ajax({
      type: 'get',
      url : '<?php echo base_url($_SESSION['hak_akses'].'/user/edit_password?id=')?>'+id,

      success: function(hasil) {
        $response = $(hasil);
        var id = $response.filter('#id').text();
        var password = $response.filter('#password').text();
        //menampilkan ke modal
        $('#edit_id').val(id);
        $('#edit_password').val(password);
        $('#modal_edit_password').modal('show');
      }
    });
  }
</script>
